FROM ubuntu:18.04
MAINTAINER FME

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install  -y  curl
RUN echo "America/New_York" > /etc/timezone
RUN apt-get -y install sqlite libsqlite3-dev libxml2-dev libxslt-dev tzdata curl openssh-server openssh-client vim git zip unzip sendmail

# SETUP RUBY
ENV RUBY_VERSION 2.5.3
ENV RVM_BUNDLE /usr/local/rvm/wrappers/ruby-$RUBY_VERSION/bundle
ENV TERM=xterm-256color

# INSTALL RUBY
RUN gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB && \
    command curl -sSL https://rvm.io/mpapis.asc | gpg --import - && \
    \curl -sSL https://get.rvm.io | bash -s stable && \
    /usr/local/rvm/bin/rvm install $RUBY_VERSION && \
    /usr/local/rvm/wrappers/ruby-$RUBY_VERSION/gem install bundler && \
    ln -s /usr/local/rvm/wrappers/ruby-$RUBY_VERSION/bundle /usr/bin/bundle

#INSTALL NODE:8
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -\
    && apt-get install -y nodejs

RUN git config --global user.name "fme-ci"
RUN git config --global user.email "dev@fullmeasureed.com"


EXPOSE 3000
